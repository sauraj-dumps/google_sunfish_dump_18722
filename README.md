## sunfish-user 12 SP2A.220305.012 8177914 release-keys
- Manufacturer: google
- Platform: sm6150
- Codename: sunfish
- Brand: google
- Flavor: sunfish-user
- Release Version: 12
- Id: SP2A.220305.012
- Incremental: 8177914
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/sunfish/sunfish:12/SP2A.220305.012/8177914:user/release-keys
- OTA version: 
- Branch: sunfish-user-12-SP2A.220305.012-8177914-release-keys
- Repo: google_sunfish_dump_18722


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
